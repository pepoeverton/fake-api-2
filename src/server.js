const jsonServer = require('json-server');
const path = require('path');

const PORT = 3000

const server = jsonServer.create();
const transactions = jsonServer.router(
  path.join(__dirname, '../database/transactions.json')
);
const middlewares = jsonServer.defaults();

server.use(middlewares);
server.use(transactions);

server.listen(PORT, () => {
  console.log(`JSON Server is running on port ${PORT}`);
  console.log(`http://localhost:${PORT}`);
});

module.exports = server;
